# rebi-cloud
rebi from cloud. tunnel needed with ros subnet.
or same subnet with robot, and rooting.


### Cloud.

1. Build rebi containers from repo. if needed.

    vke_containers.sh build rebi (változás, vagy nincs image)

    vke_containers.sh restart rebi (rviz desktop, rebi-cloud) 

    vke_containers.sh start rebi

    vke_containers.sh stop rebi

2. Set 10.10.0.0/24 network route through the robots tap0 address (tunnel), or local subnet address. 

    **sudo ip route add 10.10.0.0/24 via 172.18.0.60** 

    (ip route del ...  to delete if changed or not needed anymore.)

3. Set up tunnel. For Magnum2, check magnum2 branch.   (optional)
    **tunnel needs to be configured for every network**
    - tunsafe needs a ...$HOSTNAME.conf file. 
    - open a port from the gw to the internet to 1194 on the ros18 host machine.

        vke_containers.sh build tunnel 

        vke_containers.sh start/stop tunnel

    - use the provided client config, in magnum2 branch, edit for your GW.


### Robot
    - See rebi-onboard

### Diagnostics
    - See diagnostics with a ROS & GUI enabled computer
    - connect a remote desktop or your machine to the docker bridge network ros18
    - export ROS_MASTER_URI=http://172.18.0.175:11311 (or whatever the master is set to)
    - install rqt framework (GUI developement for ROS)
    - rosrun rqt_runtime_monitor rqt_runtime_monitor