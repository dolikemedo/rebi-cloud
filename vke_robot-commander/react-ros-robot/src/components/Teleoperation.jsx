import React, { Component } from 'react';
import { Joystick } from 'react-joystick-component';
import Config from "../scripts/config";
import base64image from '../images/joystickbase_base64string.js';

class Teleoperation extends Component {
    state = {
        ros: null
    };

    constructor(){
        super();
        this.init_connection();
        this.handleMove = this.handleMove.bind(this);
        this.handleStop = this.handleStop.bind(this);
    }

    init_connection(){
        this.state.ros = new window.ROSLIB.Ros();
        //this.setState({ros: new window.ROSLIB.Ros()});
        //console.log(this.state.ros);

        this.state.ros.on("connection", ()=> {
            console.log("connection established for Teleoperation");
            //this.setState({connected: true});
        });
        this.state.ros.on("close", ()=>{
            //console.log("connection closed");
            //this.setState({connected: false});
            //try to reconnect every RECONNECTION_TIMER seconds.
            setTimeout(()=>{
                try{
                    this.state.ros.connect("ws://"+ 
                        Config.ROSBRIDGE_SERVER_IP + 
                        ":" + 
                        Config.ROSBRIDGE_SERVER_PORT +
                        ""
                    );
                }catch(error){
                    console.log("connection problem")
                };
            }, Config.RECONNECTION_TIMER);
        }); 
        
        try{
            this.state.ros.connect("ws://"+ 
                Config.ROSBRIDGE_SERVER_IP + 
                ":" + 
                Config.ROSBRIDGE_SERVER_PORT +
                ""
            );
        }catch(error){
            console.log("connection problem")
        };
        
    }


    handleMove(event) {
        console.log("handle move");
        //create ROS publisher on the topic cmd_vel
        var cmd_vel = new window.ROSLIB.Topic({
            ros: this.state.ros,
            name: Config.CMDVEL_TOPIC,
            messageType: "geometry_msgs/Twist"
        });
        //create a Twist message to be published to ROSBridge
        var twist = new window.ROSLIB.Message({
            linear:{
                x: event.y/85,
                y: 0,
                z: 0
            },
            angular:{
                x: 0,
                y: 0,
                z: -event.x/85
            }
        });
        //publish the message on the cmd_vel topic.
        cmd_vel.publish(twist);
        //console.log(event);
    }
    handleStop() {
        console.log("handle stop");
        //create ROS publisher on the topic cmd_vel
        var cmd_vel = new window.ROSLIB.Topic({
            ros: this.state.ros,
            name: Config.CMDVEL_TOPIC,
            messageType: "geometry_msgs/Twist"
        });
        //create a Twist message to be published to ROSBridge
        var twist = new window.ROSLIB.Message({
            linear:{
                x: 0,
                y: 0,
                z: 0
            },
            angular:{
                x: 0,
                y: 0,
                z: 0
            }
        });
        //publish the message on the cmd_vel topic.
        cmd_vel.publish(twist);
        //same here, with 0 cmd_vel.
    }

    render() { 
        return (
            <div className='mb-5'>
                <Joystick
                    size={110}
                    sticky={false}
                    baseColor="#CEDEEF" 
                    stickColor="#110833" 
                    //throttle={10}
                    //minDistance={0}
                    baseImage={base64image}
                    move={this.handleMove}
                    stop={this.handleStop}>
                </Joystick>
            </div>
            
        );
    }
}
 
export default Teleoperation;