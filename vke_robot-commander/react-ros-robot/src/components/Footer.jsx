import React, { Component } from 'react';
//import {Container} from "react-bootstrap";
import {Column, Column2, Column3, Box, Row, Container} from "../styles/FooterStyles";
import hsnlab from '../images/hsnlab.png';
import tmit from '../images/tmit.png';

class Footer extends Component {     
    render() { 
        return (
            <Box>
            <Container className="text-center">
                <Row>
                <Column>
                    <p>BME - TMIT Department - HSN Laboratory - IoT, Smartcity workshop<br></br>2022 - VKE Project</p>
                </Column>
                <Column2>
                    <img src={hsnlab} height={100} width={100} alt="hsnlab"/>
                </Column2>
                <Column3>
                    <img src={tmit} height={64} width={230} alt="tmit"/>
                </Column3>
                </Row>
            </Container>
            </Box>
        );
    }
}

export default Footer;