import React, { Component } from 'react';
import Connection from './Connection';
import Teleoperation from './Teleoperation';
import Robotstate from './RobotState';
import Map from './Map';
import {Row, Col, Container } from 'react-bootstrap';

class Home extends Component {
    state = {}; 

    render() { 
        return (
            <main>
            <div>
                <Container>
                    <h2 className='text-center mt-3'>Robot Control Page</h2>
                    <Row>
                        <Col>
                            <Connection />
                        </Col>
                    </Row>
                    <Row xs={1} md={2} lg={2}>
                        <Col lg={{span:4,offset:1}}>
                            <Teleoperation />
                            <Robotstate />
                        </Col>
                        <Col>
                            <Map />
                        </Col>
                    </Row>
                </Container>
            </div>
            </main>  
        ); 
    }
}
 
export default Home;
