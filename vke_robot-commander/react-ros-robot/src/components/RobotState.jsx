import React, { Component } from 'react';
import {Row,Col,Container,Button} from 'react-bootstrap';
import Config from "../scripts/config";
import * as Three from "three";

class Robotstate extends Component {
    state = {
        ros: null,
        x:0,
        y:0,
        orientation: 0,
        linear_velocity: 0,
        angular_velocity: 0
    };

    constructor(){
        super();
        this.init_connection();
    }

    init_connection(){
        this.state.ros = new window.ROSLIB.Ros();
        //this.setState({ros: new window.ROSLIB.Ros()});
        //console.log(this.state.ros);

        this.state.ros.on("connection", ()=> {
            console.log("connection established for Robotstate");
            //this.setState({connected: true});
        });
        this.state.ros.on("close", ()=>{
            //console.log("connection closed");
            //this.setState({connected: false});
            //try to reconnect every RECONNECTION_TIMER seconds.
            setTimeout(()=>{
                try{
                    this.state.ros.connect("ws://"+ 
                        Config.ROSBRIDGE_SERVER_IP + 
                        ":" + 
                        Config.ROSBRIDGE_SERVER_PORT +
                        ""
                    );
                }catch(error){
                    console.log("Robotstate connection problem")
                };
            }, Config.RECONNECTION_TIMER);
        }); 
        try{
            this.state.ros.connect("ws://"+ 
                Config.ROSBRIDGE_SERVER_IP + 
                ":" + 
                Config.ROSBRIDGE_SERVER_PORT +
                ""
            );
        }catch(error){
            console.log("Robotstate connection problem")
        };
        
    }

    componentDidMount(){
        this.getRobotState();
    }

    getOrientationFromQuaternion(ros_orientation_quaternion){
        var q = new Three.Quaternion(
            ros_orientation_quaternion.x,
            ros_orientation_quaternion.y,
            ros_orientation_quaternion.z,
            ros_orientation_quaternion.w
        )
        //convert Quaternion to Roll, Pitch, Yaw
        var RPY = new Three.Euler().setFromQuaternion(q);

        return RPY["_z"]*(180/Math.PI);

    }

    getRobotState(){
        //create a pose subscriber
        var pose_subscriber = new window.ROSLIB.Topic({
            ros: this.state.ros,
            name: Config.POSE_TOPIC,
            messageType: "geometry_msgs/PoseWithCovarianceStamped"
        });
        //create a pose subscriber
        var odom_subscriber = new window.ROSLIB.Topic({
            ros: this.state.ros,
            name: Config.ODOM_TOPIC,
            messageType: "nav_msgs/Odometry"
        })

        //create pose callback
        pose_subscriber.subscribe((message)=>{
            this.setState({
                x: message.pose.pose.position.x.toFixed(2),
                y: message.pose.pose.position.y.toFixed(2),
                orientation: this.getOrientationFromQuaternion(message.pose.pose.orientation).toFixed(2)
            });
        });
        //create a velocity(odom) callback
        odom_subscriber.subscribe((message)=>{
            this.setState({
                //linear_velocity: message.twist.twist.linear.x.toFixed(2),
                linear_velocity: Math.sqrt(Math.pow(message.twist.twist.linear.x,2)+Math.pow(message.twist.twist.linear.y,2)).toFixed(2),
                angular_velocity: message.twist.twist.angular.z.toFixed(2)
            });
        });
        
    }


    render() { 
        return (
            <div>
                <Row>
                    <Col>
                        <h5 className='mt-2'>Position:</h5>
                        <p className='mt-0 mb-0'>x: {this.state.x} m</p>
                        <p className='mt-0 mb-0'>y: {this.state.y} m</p>
                        <p className='mt-0'>Orientation: {this.state.orientation}°</p>
                    </Col>
                </Row>
                <Row className='mt-1'>
                    <Col>
                        <h5 className='mt-2'>Velocities:</h5>
                        <p className='mt-0 mb-0'>Lin Velocity: {this.state.linear_velocity} m/s</p>
                        <p className='mt-0'>Ang Velocity: {this.state.angular_velocity} rad/s</p>             
                    </Col>
                </Row>
            </div>
        );
}};
export default Robotstate;