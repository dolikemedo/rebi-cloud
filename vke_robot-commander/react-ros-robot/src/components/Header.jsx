import React, { Component } from "react";
import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

class Header extends Component {
    render() { 
        return (
            <Container>
                <Navbar bg="dark" expand="lg" variant="dark" collapseOnSelect>
                    <Container>
                    <Navbar.Brand href="#home">React ROS Robot</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/About">About</Nav.Link>
                            <NavDropdown title="Robots" id="basic-nav-dropdown">
                                <NavDropdown.Item href="#action/3.1">Rebi 2.0</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">Mantis 1</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Mantis 2</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Mantis 3</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Pixhawk4 Drone</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="#action/3.4">Details</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                    </Container>
                </Navbar>
            </Container>
        );
    }
}
 
export default Header;