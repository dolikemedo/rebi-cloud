const Config ={
    ROSBRIDGE_SERVER_IP: "172.18.0.175",
    ROSBRIDGE_SERVER_PORT: "9090",
    RECONNECTION_TIMER: 5000, //milisec.
    CMDVEL_TOPIC: "/cmd_vel",
    POSE_TOPIC: "/initialpose",
    ODOM_TOPIC: "/odom"
}

export default Config;  