#!/bin/bash

export ROS_MASTER_URI=`cat /config/env_ROS_MASTER_URI`
export ROS_IP=`cat /config/env_ROS_IP`

source /opt/ros/melodic/setup.bash
rosrun rviz rviz &
