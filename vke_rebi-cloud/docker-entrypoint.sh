#!/bin/bash

# set -xv # Debug
# set -e # Stop on error

if [ ! -f /.dockerenv ]; then
  echo "Not running in docker!"
  exit 1
fi
source /opt/ros/melodic/setup.bash

# Start roscore
roscore &
# Waiting for roscore
until rosnode info rosout 2>/dev/null | grep Pid; do sleep 1; done

export VKE_ROBOT_URI="http://${VKE_ROBOT_HOST}:11311"

rosparam set /config/robot_name "${VKE_ROBOT_NAME}"
rosparam set /config/robot_type "rebi"
rosparam set /config/fleetmanager_uri "${VKE_FLEETMANAGER_URI}"
rosparam set /config/robot_uri "${VKE_ROBOT_URI}"
rosparam set /config/robot_host "${VKE_ROBOT_HOST}"
rosparam set /config/configstate "ready"

# Start robot
tmux new-session -d -s robot \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/catkin_ws/devel/setup.bash && roscd rebi/launch && roslaunch rebi.launch"
# Start Navigation stack
tmux new-session -d -s movebase \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/catkin_ws/devel/setup.bash; roslaunch navigation navigation.launch"
# Opening a websocket for robot_commander, or any webbased comms.
tmux new-session -d -s rosbridge \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/ros/melodic/setup.bash; roslaunch rosbridge_server rosbridge_websocket.launch"
# Teleoperation
tmux new-session -d -s teleop \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/ros/melodic/setup.bash; rosrun teleop_twist_keyboard teleop_twist_keyboard.py"
# Diagnostics
tmux new-session -d -s diagnostics \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/catkin_ws/devel/setup.bash; rosrun diagnostics aggregator.py"
# Sync parameter server between robot and cloud master.
tmux new-session -d -s sync_params \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/catkin_ws/devel/setup.bash; roslaunch sync_params sync.launch"
# Pose publisher for react app
tmux new-session -d -s robotpose_publisher \
"export ROS_MASTER_URI=http://172.18.0.175:11311 && \
export ROS_IP=172.18.0.175 && \
source /opt/catkin_ws/devel/setup.bash; rosrun robotpose_publisher robotpose_publisher"

# Start NGINX web server
sudo nginx -c /opt/vke/nginx/nginx.conf &

tail -f /dev/null
#exec "$@"