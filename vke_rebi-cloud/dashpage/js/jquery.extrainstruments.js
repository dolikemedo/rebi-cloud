(function($) {

	/*var script = document.createElement("script");
	script.src = "https://unpkg.com/leaflet@1.6.0/dist/leaflet.js";
	document.head.appendChild(script);

	var link = document.createElement("link");
	link.rel = "stylesheet";
	link.href = "https://unpkg.com/leaflet@1.6.0/dist/leaflet.css";
	document.head.appendChild(link);*/

	function StateInfo( placeholder, options ) {
		// Initial configuration
		var attitude = this;
		var settings = $.extend({
			armed : false,
			mode : ""
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big_panel"><div class="big_instrument xtrainstrument"><div class="stateinfo"><button class="stateinfo_up armed_state">ARM</button><div class="stateinfo_down">label</div></div></div></div>');
			_setArmedInfo(settings.armed);
			_setModeInfo(settings.mode);
		});

		// Private methods
		function _setArmedInfo(armed){
			placeholder.each(function(){
				e = $(this).find('.stateinfo_up')[0];
				if (armed) {
					e.innerText = "DISARM";
					$(e).removeClass("disarmed_state");
					$(e).addClass("armed_state");
				} else {
					e.innerText = "ARM";
					$(e).addClass("disarmed_state");
					$(e).removeClass("armed_state");
				}
			});
		}
		function _setModeInfo(mode){
			placeholder.each(function(){
				$(this).find('.stateinfo_down')[0].innerText = mode;
			});
		}

		// Public methods
		this.setArmedInfo = function(armed){_setArmedInfo(armed);}
		this.setModeInfo = function(mode){_setModeInfo(mode);}
		return attitude;
	};


	function DiagInstrument( placeholder, options ) {
		// Initial configuration
		var attitude = this;
		var map = new Map();
		var settings = $.extend({
			what : "what?",
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big4x_panel"><div class="big4x_instrument xtrainstrument"><div id="diag"><div style="overflow:auto"><ul id="listt"></ul><div id="chkmk" style="float:right; "><img id="diag_img"></img></div></div></div></div></div>');
			//_fine(true);


		});

		function _updatelist(list){
			var listt =document.getElementById("listt");
			listt.innerHTML="";
			_fine(true);
			list.status.forEach(function(element){
				map.set(element.name,element);
			})
			map.forEach(function(element){
				if(element.level == 0){
					listt.innerHTML +='<li style="color:Green">'+element.name+ "</li>"
				} else if(element.level==1){
					listt.innerHTML +='<li style="color:Yellow">'+element.name+ "</li>"
				} else {
					_fine(false);
					listt.innerHTML +='<li style="color:Red">'+element.name+ '<ul id="'+element.name+'"><li>'+element.message+"</li></ul></li>"
					sub=document.getElementById(element.name);
					sub.innerHTML+='<li style="color:Red">Hardware id: '+element.hardware_id+ "</li>"
					element.values.forEach(function(e){
						sub.innerhtml+='<li style="color:Red">'+e+ "</li>"
					})
				}
			})


		}
		function _fine(b){
			var img = document.getElementById("diag_img");
			if(b){
			img.src = "img/greencheckmark.png";
			} else {
			img.src = "img/red.png";
			}
			var src = document.getElementById("chkmk");
			src.innerHTML="";
			src.appendChild(img);
		}

		this.updatelist = function(list){_updatelist(list);}

		return attitude;
	};

	function PingInstrument( placeholder, options ) {
		// Initial configuration
		var attitude = this;

		var settings = $.extend({
			what : "what?",
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big4x_panel"><div class="big4x_instrument xtrainstrument"><div id="ping"><div style="float:left; width:65%; overflow:auto"><ul id="namelist" style="Display:in-line-block;"></ul></div><div style="float:right; width:35%;overflow:auto"><ul id="timelist" style="Display:in-line-block;"></ul></div></div></div></div>');



		});

		function _updatetime(list){
			var timelist =document.getElementById("timelist");
			timelist.innerHTML="";
			list.forEach(_addelementstime);
		}
		function _updatename(list){
			var namelist =document.getElementById("namelist");
			namelist.innerHTML="";
			list.forEach(_addelementsname);
		}
		function _addelementsname(item,index){
			var colo ="White";
			var list =document.getElementById("namelist");
			list.innerHTML+='<li style="color:'+colo+'">'+item+ "</li>";
		}
		function _addelementstime(item,index){
			var colo ="White";
			var list =document.getElementById("timelist");
			item2 = parseFloat(item).toFixed(5);
			if(item >1){
				colo="Yellow";
			} else if(item == -1){
				colo="Red";
			}
			list.innerHTML+='<li style="color:'+colo+'">'+item2+ " ms</li>";
		}

		this.updatetime = function(list){_updatetime(list);}
		this.updatename = function(list){_updatename(list);}


		return attitude;
	};


	function PlanInstrument( placeholder, options ) {
		// Initial configuration
		var attitude = this;
		var map;
		var drone = L.marker([0,0]);
		var settings = $.extend({
			what : "what?",
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big4x_panel"><div class="big4x_instrument xtrainstrument"><div id="map"></div></div></div>');
			map = L.map('map', {crs: L.CRS.Simple, minzoom: -3});
			drone.addTo(map);
			map.setView( [0, 0], 1);

			/*
			$('<link/>', {
				rel: 'stylesheet', type: 'text/css',
				href: 'css/L.SimpleGraticule.css'
			}).appendTo('head');

			$.getScript("js/L.SimpleGraticule.js", function(){
			*/
				var options = {
					interval: 20, showOriginLabel: true, redraw: 'move',
					zoomIntervals: [
						{start: 0, end: 3, interval: 10},
						{start: 4, end: 8, interval: 1},
						{start: 9, end: 20, interval: 0.1}
					]};
				L.simpleGraticule(options).addTo(map);
			/*
			});
			*/

		});

		// Private methods
		function _setMarker(position){
			placeholder.each(function(){
				var m = L.latLng(position);
				L.marker(m).addTo(map);
			});
		}


		function _moveDrone(position) {
			drone.setLatLng(position);
		}


		function _getMap(){
			return map;
		}






		// Public methods
		this.setMarker = function(position){_setMarker(position);}
		this.moveDrone = function(position){_moveDrone(position);}
		this.getMap = function(){return map;}

		return attitude;
	};

	function ButtonInstrument(placeholder, label, options ) {
		// Initial configuration
		var settings = $.extend({
			twostate : false,
		}, options );
		settings.label = label;

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="small_panel"><div class="small_instrument xtrainstrument"><button class="button_instrument_b">' + settings.label[0] +'</button><div class="small_instrument_label">' + settings.label + '</div></div></div>');
		});

		// Private methods
		function _setOnClick(onclick){
			placeholder.each(function(){
				var b = $(this).find(":button");
				b.on("click", onclick);
			});
		}

		// Public methods
		this.setOnClick = function(onclick){_setOnClick(onclick);}
		return this;
	};

	function Battery( placeholder, options ) {
		// Initial configuration
		var settings = $.extend({
			level : 50,
			voltage : 12,
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="halfV_panel"><div class="halfV_instrument xtrainstrument"><div class="battery_icon"><div class="battery_bar"></div><div class="battery_bar"></div><div class="battery_bar"></div><div class="battery_bar"></div><div class="battery_bar"></div></div><div class="battery_perc"></div><div class="battery_volt"</div></div></div>');

			_setLevel(settings.level, settings.voltage);
		});

		// Private methods
		function _setLevel(level, voltage){
			placeholder.each(function(){
				var index = 0;
				$(this).find('.battery_icon .battery_bar').each(function() {
					var power = Math.round(level / 20);
					var li = (4-index) * 20;
					if (li < level) {
						$(this).addClass("active");
					} else {
						$(this).removeClass("active");
					}
					index++;
				});
				ls = String(Math.round(level))+"%";
				vs = String(Math.round(voltage*10)/10)+"V";
				$(this).find('.battery_perc').html(ls);
				$(this).find('.battery_volt').html(vs);
			});
		}

		// Public methods
		this.setLevel = function(level, voltage){_setLevel(level, voltage);}

		return this;
	};


	function VSliderInstrument(placeholder, label, options ) {
		// Initial configuration
		var settings = $.extend({
			readonly : false,
			min: 0,
			max: 100,
			callback : null,
		}, options );
		settings.label = label;
		oldB = 0;
		oldY = 0;
		dPos = 0;
		nPos = 0;

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="halfV_panel"><div class="halfV_instrument xtrainstrument"><div class="vslider"></div><div class="vslider_dot">1</div><div class="small_instrument_label">' + settings.label + '</div></div></div>');
			$(this).find(".vslider_dot").mousemove(function(event){
				if (event.buttons == 1) {
					if (oldB == 1) {
						move = event.screenY - oldY;
						nPos = dPos - move;
						if (nPos > 150) nPos = 150;
						if (nPos < 30) nPos = 30;
						$(event.target).css("bottom", String(nPos)+"px");
						_setSlider(_getSlider(), false);
					} else {
						dPos = parseInt($(event.target).css("bottom"), 10);
						oldY = event.screenY;
					}
				}
				oldB = event.buttons;
			});
			$(this).find(".vslider_dot").mouseout(function(event){
				if (event.buttons == 1) {
					oldB = 0;
					val = _getSlider();
					_setSlider(val);
					if (settings.callback != null) settings.callback(val);
				}
			});
			$(this).find(".vslider_dot").mouseup(function(event){
				oldB = 0;
				val = _getSlider();
				_setSlider(val);
				if (settings.callback != null) settings.callback(val);
			});
		});

		// Private methods
		function _setSlider(slider, move = true){
			pos = 120.0 * (slider - settings.min) / (settings.max - settings.min);
			if (pos > 120) pos = 120;
			if (pos < 0) pos = 0;
			placeholder.each(function(){
				if (move) $(this).find(".vslider_dot").css("bottom", String(pos+30)+"px");
				$(this).find(".vslider_dot").text(slider);
			});
		}

		function _getSlider() {
			pos = 0;
			placeholder.each(function(){
				pos = parseInt($(this).find(".vslider_dot").css("bottom"), 10);
			});
			pos = pos - 30;
			val = Math.round(pos * (settings.max - settings.min) / 120 + settings.min);
			return val;
		}

		function _setCallback(cb) {
			settings.callback = cb;
		}

		// Public methods
		this.setSlider = function(slider){_setSlider(slider);}
		this.setCallback = function(cb){_setCallback(cb);}
		return this;
	};

	function ImageStreamInstrument(placeholder, server, topic, label, options ) {
		// Initial configuration
		var settings = $.extend({
			what : "",
		}, options );
		settings.server = server;
		settings.topic = topic;
		settings.label = label;

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big_panel"><div class="big_instrument xtrainstrument"><img width="176" height="176" style="position: relative; top: 5px; left: 5px;" src="' + settings.server + '/stream?topic=' + settings.topic + '"></img></div></div>');
		});

		// Public methods
		return this;
	};

	function HTMLInstrument(placeholder, label, options ) {
		// Initial configuration
		var settings = $.extend({
			what : "",
		}, options );
		settings.label = label;

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="big_panel"><div class="big_instrument xtrainstrument"><div class="html_instrument"></div><div class="big_instrument_label">' + settings.label + '</div></div></div>');
		});
		function _setContent(content) {
			placeholder.each(function(){
				$(this).find('.html_instrument')[0].innerHTML = content;
			});
		}

		// Public methods
		this.setContent = function(content){_setContent(content);}
		return this;
	};


	// Extension to jQuery
	$.stateInfo = function(placeholder, options) {
		return new StateInfo($(placeholder), options)
	}
	$.diagInstrument = function(placeholder, options) {
		return new DiagInstrument($(placeholder), options)
	}
	$.pingInstrument = function(placeholder, options) {
		return new PingInstrument($(placeholder), options)
	}
	$.planInstrument = function(placeholder, options) {
		return new PlanInstrument($(placeholder), options)
	}
	$.buttonInstrument = function(placeholder, label, options) {
		return new ButtonInstrument($(placeholder), label, options)
	}
	$.battery = function(placeholder, options) {
		return new Battery($(placeholder), options)
	}
	$.vSliderInstrument = function(placeholder, label, options) {
		return new VSliderInstrument($(placeholder), label, options)
	}
	$.imageStreamInstrument = function(placeholder, topic, label, options) {
		return new ImageStreamInstrument($(placeholder), topic, label, options)
	}
	$.htmlInstrument = function(placeholder, label, options) {
		return new HTMLInstrument($(placeholder), label, options)
	}

	$.fn.stateInfo = function(data, options) {
		return this.each(function(){ $.stateInfo(this, options); });
	}
	$.fn.diagInstrument = function(data, options) {
		return this.each(function(){ $.diagInstrument(this, options); });
	}
	$.fn.pingInstrument = function(data, options) {
		return this.each(function(){ $.pingInstrument(this, options); });
	}
	$.fn.planInstrument = function(data, options) {
		return this.each(function(){ $.planInstrument(this, options); });
	}
	$.fn.buttonInstrument = function(data, label, options) {
		return this.each(function(){ $.buttonInstrument(this, label, options); });
	}
	$.fn.battery = function(data, options) {
		return this.each(function(){ $.battery(this, options); });
	}
	$.fn.vSliderInstrument = function(data, options) {
		return this.each(function(){ $.vSliderInstrument(this, options); });
	}
	$.fn.imageStreamInstrument = function(data, topic, label, options) {
		return this.each(function(){ $.imageStreamInstrument(this, topic, label, options); });
	}
	$.fn.htmlInstrument = function(data, label, options) {
		return this.each(function(){ $.htmlInstrument(this, label, options); });
	}

}( jQuery ));
