(function($) {
	function Thrust( placeholder, options ) {
		// Initial configuration
		var attitude = this;
		var settings = $.extend({
			ts : [0, 500, 1000, 1500]
		}, options );

		// Creation of the instrument
		placeholder.each(function(){
			$(this).html('<div class="thrust_panel"><div class="thrust"></div><div class="thrust_nums"></div></div>');
			inner1 = $(this).find('div.thrust');
			inner2 = $(this).find('div.thrust_nums');
			for (i = 0; i<4; i++) {
				inner1.append('<div class="thrust_bar"><progress value="100" max="2000" style="width: 140px"></progress></div>');
				inner2.append('<div class="thrust_num" style="text-align: center; width: 100px;">0</div>');
			}
			_setThrust(settings.ts);
		});
			
		// Private methods
		function _setThrust(ts){
			placeholder.each(function(){
				var index = 0;
				$(this).find('.thrust .thrust_bar').each(function() {
					$(this).find("progress")[0].value = ts[index];
					index = index + 1;
				});
				var index = 0;
				$(this).find('.thrust_num').each(function() {
					$(this)[0].innerHTML = ts[index];
					index = index + 1;
				});
			});
		}

		// Public methods
		this.setThrust = function(ts){_setThrust(ts);}

		return attitude;
	};
		
	// Extension to jQuery
	$.thrust = function(placeholder, options){
		var thrust = new Thrust($(placeholder), options)
		return thrust;
	}

	$.fn.thrust = function(data, options){
		return this.each(function(){
			$.thrust(this, options);
		});
	}
}( jQuery ));