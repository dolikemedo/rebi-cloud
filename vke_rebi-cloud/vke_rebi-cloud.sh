#!/bin/sh

CONFIG=$2
CONFIG2=$3

MY_PATH="`dirname \"$0\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"
. $MY_PATH/../vke_common/funcs.sh

vke_config_read rebi-cloud ${CONFIG} ${CONFIG2}

vke_run "$@"
