#!/usr/bin/env python3


import math, sys, time, signal, socket
import numpy as np
#import roslib; roslib.load_manifest('rebipython')
import roslib, rospy
from geometry_msgs.msg import Twist
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue


def signal_handler(sig, frame):    
    print("\n CheckBattery shutdown! \n")
    sys.exit(0)

# Variables for diagnostics.
lastCmdvel = 0
firstCmdvel = False
State = False

def aggregate(msg):
    global lastCmdvel
    #Reacting to different diagnostics:
    if msg.status[0].name == "Hebi Battery":
        print("Battery")
        #print(type(msg.status[0].values[1]))
        #print(msg.status[0].values[1])

    elif msg.status[0].name == "rebiControl Diagnostics":
        actualCmdvel = float(msg.status[0].values[0].value)
        print(type(msg.status[0].values[0]))
        print(f"actual: {actualCmdvel}")
        print(f"last: {lastCmdvel}")
        if actualCmdvel - lastCmdvel > 30 and lastCmdvel != 0:
            print("nagyobb 30 mp")
        lastCmdvel = actualCmdvel


def send_diags(diagnostics_pub):

    #global firstCmdvel

    # See diagnostics with: rosrun rqt_runtime_monitor rqt_runtime_monitor
    msg = DiagnosticArray()
    msg.status = []
    msg.header.stamp = rospy.Time.now()
    
    # if firstCmdvel:
    #     firstCmdvel = False


    # if now - lastCmdvel > 6.0:
    #     Robot = "Idle"
    #     State = False


    # if State:
    #     Robot = "Performing"
    # el:
    #     Robot = "Idle"

    status = DiagnosticStatus()
    status.name = f"AGV-Cloud Diagnostics"
    status.hardware_id = f"{socket.gethostname()} AGV-Cloud of IoTLab"
    status.values.append(KeyValue("Rebi got cmd_vel?", f"{1234}"))

    # if isnan(Robot):
    #     status.level = DiagnosticStatus.ERROR
    #     status.message = f"Measurement Error, ADC or I2C problems."
    # elif BS.percentage < 0.23:
    #     status.level = DiagnosticStatus.ERROR
    #     status.message = f"Battery might over discharge! Warning!"
    # elif BS.percentage < 0.28 and BS.percentage > 0.23:
    #     status.level = DiagnosticStatus.WARN
    #     status.message = f"Battery level near safe minimum!"
    # else:
    #     status.level = DiagnosticStatus.OK
    #     status.message = 'OK'

    status.level = DiagnosticStatus.OK
    status.message = 'Im sending Diags.' 
    msg.status.append(status)
    diagnostics_pub.publish(msg)
    #rospy.loginfo(f"status.message: {status.message}")

def run():
    global now 

    rospy.init_node('agv_diagnostics_aggregator')
    rospy.Subscriber("/diagnostics", DiagnosticArray, aggregate)
    #rospy.Subscriber("odom", Twist, callback) or missing odom ? mennyire kell részletesen diagnosztizálni? 
    #rospy.Subscriber("", Twist, callback)
    rate = rospy.Rate(0.1) #mp-enként küld diagot. 
    diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)

    lastDiagnostics = 0

    while not rospy.is_shutdown():
        now = rospy.get_time()
        #if now - lastDiagnostics > 1.0 or firstCmdvel:
        try:
            send_diags(diagnostics_pub)
            lastDiagnostics = now
        except Exception as e:
            rospy.logerr("Exception while reading diagnostics: %s"%e)
        
        rate.sleep() #Itt talán elég lenne a rate-el beállítani a diag periódusát.


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        run()
    except rospy.ROSInterruptException:
        passs