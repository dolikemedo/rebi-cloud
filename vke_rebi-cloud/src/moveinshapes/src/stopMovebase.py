#!/usr/bin/env python3

from paho.mqtt import client as mqtt_client
from datetime import datetime, timedelta
import time, random, signal, sys

import rospy
from std_msgs.msg import String
from actionlib_msgs.msg import GoalID

broker = '172.18.0.60'
port = 1883
subtopic = "Rebi/Stop"
client_id = f'python-mqtt-{random.randint(0, 1000)}'

state = False

##-- Ctrl+C handler --##

def signal_handler(sig, frame):
    print('\nstopbutton_node shutdown')
    client.loop_stop()
    sys.exit(0)


##-- MQTT & ROS --##

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    # Set Connecting Client ID
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username="rebi", password="cicamica")
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        global state
        stateread = int(msg.payload.decode())
        print(f"Received `{stateread}` from `{msg.topic}` topic")
        if stateread:
            state = True
        else: 
            if (state):
                print("\nButton Released!\n")
            state = False
            

    client.subscribe(subtopic)
    client.on_message = on_message


def run():
    global client
    client = connect_mqtt()
    subscribe(client)
    #client.loop_forever() atmegy ide a process
    client.loop_start()

    rospy.init_node('stopbutton_node', anonymous=True)
    pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=10)

    counter = 0
    stop = GoalID()


    while True:
        if state:
            print(f"state: {state}")
            stop_str = f"STOP message {counter}"
            print("Button Pushed!\n")
            counter += 1
            rospy.loginfo(stop_str)
            pub.publish(stop)  #send 0 goal
            time.sleep(1)

        # elif not state:
        #     print("Button Released!\n")    



if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    try:
        run()
    except rospy.ROSInterruptException:
        print("Something ROS wrong")
    



#rostopic pub /move_base/cancel actionlib_msgs/GoalID -- {}