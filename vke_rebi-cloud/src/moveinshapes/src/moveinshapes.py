#!/usr/bin/env python3

import time, random, signal, sys

import rospy
from std_msgs.msg import String
from actionlib_msgs.msg import GoalID

state = True

def signal_handler(sig, frame):
    print('\nmoveinshape node shutdown')
    sys.exit(0)


def run():

    counter = 0
    #----Watching currentGoal and Matching Shapepoints----#
    while True:
        if state: #and now - lastCheck > 1000 :

            print(f"state: {state}")
            stop_str = f"STOP message {counter}"
            print("Button Pushed!\n")
            counter += 1
            rospy.loginfo(stop_str)
            pub.publish(stop)  #send 0 goal
            time.sleep(1)
        else: 
            print(f"state: {state}")
    

def stop_callback(message):
    global state # true active, false standby
    if message.data == "enable":
        state = True
    elif message.data == "stop":
        state = False


def command_callback(message):


    #os.system('clear')
    print("The command was: ", message)


    def calcSprial(edges, dir, res):   #numberofedges, direction, resolution

    def calcPolygon(edges, dir, res):   #numberofedges, direction, resolution
        

    if state == True:

        print(f"Calculating '{message.shape}' points")

        if message.shape == "stop":
            # Delete points, stop running procces. terminate.

        elif message.shape == "circle left":
            # circle.angular.z = (7/10)*message.speed
            # moveCircle(circle)
            # calculate points of a Circle with a certain resolution.
        elif message.shape == "circle right":
            # calculate points of a Circle with a certain resolution.
            # calcPolygon(HIGHnumberOfedges, direction, HIGHresolution)
        elif message.shape == "square left":
            # calculate points of a Square with a certain resolution.
            # calcPolygon(numberOfedges, direction, resolution)
        elif message.shape == "square right":
            # calculate points of a Square with a certain resolution.
        elif message.shape == "triangle left":
            # calculate points of a Triangle with a certain resolution.
            # calcPolygon(numberOfedges, direction, resolution)
        elif message.shape == "triangle right":
            # calculate points of a Triangle with a certain resolution.
            # calcPolygon(numberOfedges, direction, resolution)
        elif message.shape == "spiral left":
            # calculate points of a Sprial with a certain resolution.
            # calcSpiral(numberOfpoints, direction, resolution)
        elif message.shape == "spiral right":
            # calculate points of a Sprial with a certain resolution.
            # calcSpiral(numberOfpoints, direction, resolution)

        #------Give rosparams of shape -------------#
        # 
        


        #------Launch moveinshape.py, popen. -------#
        # 

    else: 
        print("movebase is disabled by constant empty Goal. Robot stopped")   #ide belerakni a rebi nevét.

    

def listener():
    rospy.init_node('moveinshapes node')
    rospy.Subscriber("shapes", shapespeed, command_callback)
    rospy.Subscriber("rebi/stop", String, stop_callback)
    rospy.spin()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    try:
        listener()
        run()
    except rospy.ROSInterruptException:
        print("Something ROS wrong")