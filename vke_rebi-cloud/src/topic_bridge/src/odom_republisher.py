#!/usr/bin/env python

import rospy
import tf
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
import argparse
import sys

args = None

def odom_callback(data):
    global args

    # publish transforms over tf
    odom_broadcaster.sendTransform(
        (data.pose.pose.position.x, data.pose.pose.position.y, data.pose.pose.position.z),
        (data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w),
        data.header.stamp, args.child_frame_id, args.frame_id)

    # publish to obom topic
    odom = Odometry()
    odom.header.stamp = data.header.stamp
    odom.header.frame_id = args.frame_id

    # set the position
    odom.pose.pose = Pose(
        Point(data.pose.pose.position.x, data.pose.pose.position.y, data.pose.pose.position.z),
        Quaternion(data.pose.pose.orientation.x, data.pose.pose.orientation.y, data.pose.pose.orientation.z, data.pose.pose.orientation.w))

    # set the velocity
    odom.child_frame_id = args.child_frame_id
    odom.twist.twist = Twist(
        Vector3(data.twist.twist.linear.x, data.twist.twist.linear.y, data.twist.twist.linear.z),
        Vector3(data.twist.twist.angular.x, data.twist.twist.angular.y, data.twist.twist.angular.z))

    # publish the message
    odom_pub.publish(odom)

if __name__ == '__main__':
    CLI=argparse.ArgumentParser(description='Create odom frames for tf.')
    CLI.add_argument("--frame_id", required=True, help='Frame id for the tf and odom topics')
    CLI.add_argument("--child_frame_id", required=True, help='Child frame id for the tf and odom topics')
    args = CLI.parse_args(rospy.myargv(argv=sys.argv)[1:])

    rospy.init_node('odom_republisher')

    odom_pub = rospy.Publisher("odom_out", Odometry, queue_size=5)
    odom_broadcaster = tf.TransformBroadcaster()
    rospy.Subscriber("odom", Odometry, odom_callback)

    rospy.spin()
