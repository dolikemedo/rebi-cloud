#!/usr/bin/env python

from xmlrpclib import ServerProxy
import time
import argparse
import os
import sys
from datetime import datetime, timedelta

msOther = None
msHost = None
activePublishers = {}
activePublisherStore = {}
prefixName = "proxy"

def addPublishers(pubs, topic):
    global msOther, msHost
    global activePublishers, activePublisherStore
    global prefixName

    for node in pubs:
        (code, statusMessage, URI) = msOther.lookupNode('/', node)
        if code < 0:
            continue
        (code, statusMessage, topicTypes) = msOther.getTopicTypes('/')
        if code < 0:
            continue
        topicType = None
        for [tName, tType] in topicTypes:
            if tName == topic:
                topicType = tType
        if topicType is None:
            continue

        print("Add a publisher for topic {} ({}): {}".format(topic, topicType, URI))
        nodeName = '/' + prefixName + "_" + topic.replace('/','_') + "_" + node.replace('/','_')
        msHost.registerPublisher(nodeName, topic, topicType, URI)
        activePublishers[topic].append(node)
        activePublisherStore[(node, topic)] = (nodeName, topic, topicType, URI)

def delPublishers(pubs, topic):
    global msOther, msHost
    global activePublishers, activePublisherStore
    for node in pubs:
        nodeName, topic, topicType, URI = activePublisherStore[(node, topic)]
        print("Remove a publisher for topic {} ({}): {}".format(topic, topicType, URI))
        msHost.unregisterPublisher(nodeName, topic, URI)
        activePublishers[topic].remove(node)
        del activePublisherStore[(node, topic)]

def proxy(master, topics, polltime):
    global msOther, msHost, activePublishers

    olde = None
    while True:
        try:
            msOther = ServerProxy(master)
            msOther.getSystemState('/')
            msHost = ServerProxy(os.environ['ROS_MASTER_URI'])
            msHost.getSystemState('/')
            break
        except Exception as e:
            if "{}".format(e) != olde:
                print("Waiting for roscores! {}".format(e))
                olde = "{}".format(e)
                
    print("Ready to proxy topics")

    while True:
        dt = datetime.now()
        dtt = dt.second + dt.microsecond / 1000000.0

        try:
            (code, statusMessage, systemState) = msOther.getSystemState('/')
            if code > 0:
                [publishers, subscribers, services] = systemState
                for topic in topics:
                    if topic not in activePublishers:
                        activePublishers[topic] = []

                    topicEmpty = True
                    for top, toppubs in publishers:
                        #print(top, toppubs)
                        if top == topic:
                            topicEmpty = False
                            addPublishers(list(set(toppubs)-set(activePublishers[topic])), topic)
                            delPublishers(list(set(activePublishers[topic])-set(toppubs)), topic)
                    if topicEmpty:
                        delPublishers(activePublishers[topic], topic)
        except:
            pass

        # Wait for the next try
        dt2 = datetime.now()
        dtt2 = dt2.second + dt2.microsecond / 1000000.0
        dtw = dtt + polltime - dtt2
        if (dtw < 0):
            dtw = 0
        time.sleep(dtw)


if __name__ == '__main__':
    CLI=argparse.ArgumentParser(description='Subscription proxy, get a topic from anouther master.')
    CLI.add_argument("--other", required=True, help='ROS Master URI for the other roscore')
    CLI.add_argument("--topics", required=True, nargs="*", help='Topics to bridge')
    CLI.add_argument("--poll", required=False, default=1.0, help='Poll interval for changes')
    CLI.add_argument("--nodeprefix", required=False, default="proxy", help='Prefix for the ghost node name')
    args, unknown = CLI.parse_known_args()

    prefixName = args.nodeprefix
    proxy(args.other, args.topics, args.poll)
