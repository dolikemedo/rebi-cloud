#!/usr/bin/env python

import rospy
import time
from geometry_msgs.msg import Twist, TwistStamped

def callback(cmdVelocity):

    baseVelocity = TwistStamped()
    baseVelocity.twist = cmdVelocity
    now = rospy.get_rostime()
    baseVelocity.header.stamp.secs = now.secs
    baseVelocity.header.stamp.nsecs = now.nsecs

    baseVelocityPub.publish(baseVelocity)

if __name__ == '__main__':
    rospy.init_node('cmd_vel_listener', anonymous=True)
    baseVelocityPub = rospy.Publisher('cmd_vel_stamped', TwistStamped, queue_size=5)
    rospy.Subscriber("cmd_vel_raw", Twist, callback)
    rospy.spin()
