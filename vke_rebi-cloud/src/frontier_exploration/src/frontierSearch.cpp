#include <costmap_2d/cost_values.h>
#include <costmap_2d/costmap_2d.h>
#include <explore/costmap_tools.h>
#include <explore/frontierSearch.h>
#include <geometry_msgs/Point.h>
#include <stdio.h>

#include <mutex>

namespace frontier_exploration
{
using costmap_2d::FREE_SPACE;
using costmap_2d::LETHAL_OBSTACLE;
using costmap_2d::NO_INFORMATION;

FrontierSearch::FrontierSearch(costmap_2d::Costmap2D* costmap,
                               double potential_scale, double gain_scale,
                               double min_frontier_size)
  : costmap_(costmap)
  , potential_scale_(potential_scale)
  , gain_scale_(gain_scale)
  , min_frontier_size_(min_frontier_size)
{
  size_x_ = costmap_->getSizeInCellsX();
  size_y_ = costmap_->getSizeInCellsY();
  frontier_flag.resize(size_x_ * size_y_, false);
  visited_flag.resize(size_x_ * size_y_, false);
}

std::vector<Frontier> FrontierSearch::searchFrom(geometry_msgs::Point position)
{
  std::vector<Frontier> fr;
  std::vector<unsigned int> frontiersToDelete;
  std::vector<unsigned int> activeAreaFrontierIndexes;

  if (!isPointOnMap(position)) {
    return fr;
  }

  int posIndex = getIndex(position);

  std::lock_guard<costmap_2d::Costmap2D::mutex_t> lock(*(costmap_->getMutex()));
  map_ = costmap_->getCharMap();  //így megkapjuk az egyes cellák állapotát(free
                                  // space, no information...)

  std::queue<unsigned int> bfs;

  unsigned int clear;
  if (frontier_points.size() == 0) {
    if (nearestCell(clear, posIndex, FREE_SPACE, *costmap_)) {
      bfs.push(clear);

    } else {
      bfs.push(posIndex);
      ROS_WARN("Could not find nearby clear cell to start search");
    }
  } else {
    for (int i = 0; i < frontier_points.size(); i++) {
      if (insideActiveArea(frontier_points[i], position) && (isNewFrontierCell(frontier_points[i]) == false) ) {
        bfs.push(frontier_points[i]);
        activeAreaFrontierIndexes.push_back(i);
        visited_flag[frontier_points[i]] = false;
      }
    }
  }

  unsigned int activeFrontiersIndex = 0;
  while (!bfs.empty()) {
    unsigned int i = bfs.front();
    bfs.pop();
    //visited_flag[i] = true;
    if ((map_[i] == FREE_SPACE)) {
      //visited_flag[i] = true;
      if ((frontier_flag[i] == false) && (isNewFrontierCell(i) == true)) {
        frontier_points.push_back(i);
        frontier_flag[i] = true;
        //visited_flag[i] = true;

      } else if (frontier_flag[i] == true) {    // ez így valószínűleg nem lesz jó, mert
                                                // lehet az aktív területben olyan
                                                // frontier, ami frontier is maradt még
        //if(isNewFrontierCell(i) == false){
          frontier_flag[i] = false;
          frontiersToDelete.push_back(
              activeAreaFrontierIndexes[activeFrontiersIndex]);
        //}
        if(activeFrontiersIndex < activeAreaFrontierIndexes.size()){
          activeFrontiersIndex++; 
        }                             
        visited_flag[i] = true;
      }
      for (unsigned int nbr : nhood4(i, *costmap_)) {
        if ((map_[nbr] <= map_[i]) && (insideActiveArea(nbr, position) ==true) && (visited_flag[nbr] == false)) {
          bfs.push(nbr);
          visited_flag[nbr] = true;
        }
      }
    }
  }
  ROS_DEBUG("ennyi frontier pont: %ld ", frontier_points.size());
  if (frontiersToDelete.empty() == false) {
    int k = frontiersToDelete.size() - 1;
    for (int i = frontier_points.size() - 1; i >= 0; i--) {
      if (i == frontiersToDelete[k]) {
        frontier_points.erase(frontier_points.begin() + i);
        k--;
      }
    }
  }
  std::vector<Frontier> frontiers =
      buildNewFrontiers(posIndex, frontier_points);
  return frontiers;
}

std::vector<Frontier> FrontierSearch::buildNewFrontiers(
    int reference, std::vector<unsigned int> frontier_points_param)
{
  std::vector<bool> checked(frontier_points_param.size(), false);
  geometry_msgs::Point reference_point = getWorldPoint(reference);

  std::vector<Frontier> frontierSets;
  std::vector<unsigned int> stack;

  for (int idx = 0; idx < frontier_points_param.size(); idx++) {
    if (checked[idx] == false) {
      Frontier f = initializenewFrontier(frontier_points_param[idx]);
      stack.push_back(frontier_points_param[idx]);
      checked[idx] = true;

      while (!stack.empty()) {
        unsigned int mapIndex = stack.front();
        stack.erase(stack.begin());
        for (unsigned int i : nhood8(mapIndex, *costmap_)) {
          for (int j = 0; j < frontier_points_param.size(); j++) {
            if ((i == frontier_points_param[j]) && (checked[j] == false)) {
              checked[j] = true;
              addNewPointToFrontier(f, frontier_points_param[j],
                                    reference_point);
              stack.push_back(i);
            }
          }
        }
      }

      if (f.size * costmap_->getResolution() >= min_frontier_size_) {
        ROS_DEBUG("el lett dobva egy frontier, mert kicsi volt");
        f.centroid.x /= f.size;
        f.centroid.y /= f.size;
        frontierSets.push_back(f);
      }
  }
  }
  for (auto& frontier : frontierSets) {
    frontier.cost = frontierCost(frontier);
  }
  std::sort(
      frontierSets.begin(), frontierSets.end(),
      [](const Frontier& f1, const Frontier& f2) { return f1.cost < f2.cost; });

  return frontierSets;
}

bool FrontierSearch::isNewFrontierCell(unsigned int idx)
{
  if( (map_[idx] == LETHAL_OBSTACLE) || (map_[idx] == NO_INFORMATION)){
    return false;
  }
  
  for (unsigned int i :
       nhood4(idx, *costmap_)) {      // ha van egy cella ami free space és van
                                      // legalább egy szomszédja, ami
    if ((map_[i] == NO_INFORMATION) && (map_[i] != LETHAL_OBSTACLE)) {  //állapota NO_iNFORMATION, akkor az a
                                      // cella frontier
      return true;
    }
  }
  return false;
}

bool FrontierSearch::isPointOnMap(geometry_msgs::Point pos)
{
  unsigned int mapx, mapy;
  if (!costmap_->worldToMap(pos.x, pos.y, mapx, mapy)) {
    ROS_ERROR("A robot a térkép határain kívül helyezkedik el");
    return false;
  }
  return true;
}

bool FrontierSearch::insideActiveArea(unsigned int idx,
                                      geometry_msgs::Point robotPose)
{

  if((map_[idx] == LETHAL_OBSTACLE) || (map_[idx] == NO_INFORMATION)){
    return false;
  }

  geometry_msgs::Point p = getWorldPoint(idx);

  // most az egyszerűség kedcéért négyzet alakú lesz az active area
  float dx = p.x - robotPose.x;
  float dy = p.y - robotPose.y;
  // ROS_DEBUG("asdasdasd %f %f", dx, dy);
  if (((dx * dx) + (dy * dy)) <= (activeAreaRadius * activeAreaRadius)) {
    // ROS_DEBUG("benne van benne van");
    return true;
  } else {
    // ROS_DEBUG("nincs benne nincs benne");
    return false;
  }
}

int FrontierSearch::getIndex(geometry_msgs::Point point)
{
  unsigned int mx, my;
  double wx = point.x;
  double wy = point.y;
  costmap_->worldToMap(wx, wy, mx, my);
  int index = costmap_->getIndex(mx, my);
  return index;
}
Frontier FrontierSearch::initializenewFrontier(unsigned int firstPointIndex)
{
  Frontier f;
  f.centroid.x = 0;
  f.centroid.y = 0;
  f.size = 1;
  f.min_distance = std::numeric_limits<double>::infinity();
  geometry_msgs::Point initial_point = getWorldPoint(firstPointIndex);
  f.initial.x = initial_point.x;
  f.initial.y = initial_point.y;
  return f;
}

void FrontierSearch::addNewPointToFrontier(Frontier& f,
                                           unsigned int newPointIndex,
                                           geometry_msgs::Point ref_point)
{
  geometry_msgs::Point p;
  p = getWorldPoint(newPointIndex);
  f.points.push_back(p);
  f.size++;
  f.centroid.x += p.x;
  f.centroid.y += p.y;
  double distance = pow((double(ref_point.x) - double(p.x)), 2.0) +
                    pow((double(ref_point.y) - double(p.y)), 2.0);
  if (distance < f.min_distance) {
    f.min_distance = distance;
    f.middle.x = p.x;
    f.middle.y = p.y;
  }
}

geometry_msgs::Point FrontierSearch::getWorldPoint(unsigned int index)
{
  geometry_msgs::Point p;
  unsigned int mx, my;
  double wx, wy;
  costmap_->indexToCells(index, mx, my);
  costmap_->mapToWorld(mx, my, wx, wy);
  p.x = wx;
  p.y = wy;
  return p;
}

double FrontierSearch::frontierCost(const Frontier& frontier)
{
  return (potential_scale_ * frontier.min_distance *
          costmap_->getResolution()) -
         (gain_scale_ * frontier.size * costmap_->getResolution());
  // return frontier.min_distance;
}
}  // namespace frontier_exploration