export USER=`id -u -n`
export VKE_PATH=${VKE_PATH:-$MY_PATH/..}

vke_config_read() {
  cfgfile=`mktemp`
  section=$1
  file=$2
  if [ -n "$3" ]; then section=${section}_$3; fi
  if [ -z "$2" ]; then file=default; fi 
  sed -n "/^### ${section}$/,/###/p" ${VKE_PATH}/vke_config/${file} > $cfgfile
  if [ -s $cfgfile ]; then
    . $cfgfile
  else
    echo "No such section ($section) in config file ($file)"
  fi
  rm -f $cfgfile
}

vke_build() {
  if [ -z "${IMAGE}" ]; then
    echo "Cannot build noname image!"
  else
    docker build -t ${IMAGE} .
  fi
}

vke_rebuild() {
  if [ -z "${IMAGE}" ]; then
    echo "Cannot rebuild noname image!"
  else
    docker build --pull --no-cache -t ${IMAGE} . 
  fi
}

vke_start() {
  OPTS="${NVIDIAEXT} --name ${CONTAINER} --hostname ${CONTAINER} --network ${ROSNET}"
  [ ! -z "${CIP}" ] && OPTS="${OPTS} --ip ${CIP}" && ROS_IP="${ROS_IP:-$CIP}"
  [ ! -z "${ROS_IP}" ] && OPTS="${OPTS} -e ROS_IP=${ROS_IP}"
  [ ! -z "${HMOUNT}" ] && OPTS="${OPTS} -v ${HMOUNT}:${LMOUNT}"
  [ ! -z "${ROS_MASTER_URI}" ] && OPTS="${OPTS} -e ROS_MASTER_URI=${ROS_MASTER_URI}"
  [ ! -z "${LOCAL_USER_ID}" ] && OPTS="${OPTS} -e LOCAL_USER_ID=${LOCAL_USER_ID}"

  for e in `printenv | cut -d= -f 1 | grep VKE_`; do
    if [ "$e" != "VKE_PATH" ]; then
      #v=`printenv $e`
      #OPTS="${OPTS} -e ${e}=\'${v}\'"
      OPTS="${OPTS} -e ${e}"
    fi
  done
  docker create ${OPTS} ${XTRAOPTS} ${IMAGE}
  docker start ${CONTAINER}
}

vke_stop() {
  docker kill ${CONTAINER}
  docker rm ${CONTAINER}
}

vke_exec() {
  docker exec -it ${CONTAINER} bash
}

vke_run() {
  cd ${MY_PATH}
  case "$1" in

    start)
      echo "Starting ${CONTAINER} docker container"
      vke_start
      ;;

    stop)
      echo "Stopping ${CONTAINER} docker container"
      vke_stop
      ;;

    restart)
      echo "Restartting ${CONTAINER} docker container"
      vke_stop
      vke_start
      ;;

    enter)
      echo "Entering ${CONTAINER} docker container"
      vke_exec
      ;;

    build)
      vke_build
      ;;

    rebuild)
      vke_rebuild
      ;;

    compile)
      vke_build
      ;;

    *)
      echo "Usage: $0 start|stop|restart|enter|build|rebuild"
      exit 1
      ;;

  esac
}
