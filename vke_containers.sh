#!/bin/sh

CONFIG=$2
CONFIG2=$3

MY_PATH="`dirname \"$0\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"
VKE_PATH=$MY_PATH
. $MY_PATH/vke_common/funcs.sh

vke_config_read containers ${CONFIG} ${CONFIG2}

env_cleanup() {
  for e in "CONTAINER_DIR CONTAINER_TAG CIP ROS_IP HMOUNT LMOUNT ROS_MASTER_URI LOCAL_USER_ID NVIDIAEXT"; do
    unset ${e}
  done
  for e in `printenv | cut -d= -f 1 | grep VKE_`; do
    if [ "$e" != "VKE_PATH" ]; then
      unset ${e}
    fi
  done
}


vke_start() {
  for c in $CONTAINERS; do
    env_cleanup
    vke_config_read ${c} ${CONFIG} ${CONFIG2}
    if [ -z "$CONTAINER_DIR" ]; then
      CONTAINER_DIR=vke_$c
    fi
    if [ -z "$CONTAINER_TAG" ]; then
      CONTAINER_TAG=$CONFIG2
    fi
    ./${CONTAINER_DIR}/${CONTAINER_DIR}.sh start $CONFIG $CONTAINER_TAG
  done
}

vke_stop() {
  for c in $CONTAINERS; do
    env_cleanup
    vke_config_read ${c} ${CONFIG} ${CONFIG2}
    if [ -z "$CONTAINER_DIR" ]; then
      CONTAINER_DIR=vke_$c
    fi
    if [ -z "$CONTAINER_TAG" ]; then
      CONTAINER_TAG=$CONFIG2
    fi
    ./${CONTAINER_DIR}/${CONTAINER_DIR}.sh stop $CONFIG $CONTAINER_TAG
  done
}

vke_build() {
  for c in $CONTAINERS; do
    env_cleanup
    vke_config_read ${c} ${CONFIG} ${CONFIG2}
    if [ -z "$CONTAINER_DIR" ]; then
      CONTAINER_DIR=vke_$c
    fi
    if [ -z "$CONTAINER_TAG" ]; then
      CONTAINER_TAG=$CONFIG2
    fi
    ./${CONTAINER_DIR}/${CONTAINER_DIR}.sh build $CONFIG $CONTAINER_TAG
  done
}

case "$1" in

  build)
    echo "Building docker containers"
    vke_build
    ;;

  start)
    echo "Starting docker containers"
    vke_start
    echo "Container is running"
    ;;

  stop)
    echo "Stopping docker containers"
    vke_stop
    ;;

  restart)
    echo "Restartting docker containers"
    vke_stop
    vke_start
    ;;

  *)
    echo "Usage: $0 start|stop|restart [config] [subconfig]"
    exit 1
    ;;

esac
