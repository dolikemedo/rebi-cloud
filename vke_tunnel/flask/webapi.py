from flask import Flask
from markupsafe import escape
import socket

addr1 = socket.gethostbyname('google.com')

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'VKE Tunnel API'

@app.route('/api/lookup/<hostname>')
def lookup(hostname):
    try:
        addr1 = socket.gethostbyname(hostname)
        return addr1
    except:
        return ''

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
