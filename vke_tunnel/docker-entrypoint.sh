#!/bin/bash

if [ ! -f /.dockerenv ]; then
  echo "Not running in docker!"
  exit 1
fi

#exec "$@"
#tail -f /dev/null

case "${HOSTNAME}" in 
  vke*)
    br="br0"
    tap="tap0"
    eth="eth0"
    br_ip="172.18.0.101/24"

    openvpn --mktun --dev $tap
    brctl addbr $br
    brctl addif $br $eth
    brctl addif $br $tap
    ifconfig $eth 0.0.0.0 promisc up
    ifconfig $tap 0.0.0.0 promisc up
    ifconfig $br $br_ip
    route add default gw 172.18.0.1 $br

    sysctl net.ipv4.ip_forward=1
    tmux new -d -s tunsafe "/usr/bin/tunsafe start /config/tunsafe/tunsafe_peer_${HOSTNAME}.conf"
    tmux new -d -s openvpn "cd /config/openvpn/server && openvpn server.conf" 
    ;;

  service*)
    echo "Starting service configuration"
    # eth0 is the interface for the cloud services. tap0 is the tunnel to the external service (could be a bridge)
    # eth1 is the interface to the host. Some service is available on the host's port
    br="br0"
    tap0="tap0"
    eth="eth0"
    br_ip=`hostname -i`
    defaultip=`ip route | grep default | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"`

    openvpn --mktun --dev $tap0
    brctl addbr $br
    brctl addif $br $eth $tap0
    ifconfig $eth 0.0.0.0 promisc up
    ifconfig $tap0 0.0.0.0 promisc up
    ifconfig $br $br_ip

    sysctl net.ipv4.ip_forward=1
    iptables -t nat -A PREROUTING -p tcp -d 172.21.0.1 -m multiport --dports 10001:19999 -j DNAT --to-destination $defaultip
    # Port 9000 is for portainer
    iptables -t nat -A PREROUTING -p tcp -d 172.21.0.1 --dport 9000 -j DNAT --to-destination $defaultip
    iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE

    echo "server-bridge ${br_ip} 255.255.0.0 172.21.0.200 172.21.0.254" >> /config/openvpn/server/server_service_udp.conf
    tmux new -d -s openvpn_udp "cd /config/openvpn/server && openvpn server_service_udp.conf" 
    tmux new -d -s webapi "python3 /opt/vke/flask/webapi.py"
    ;;
esac

tail -f /dev/null
