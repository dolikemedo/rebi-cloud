#!/bin/sh

IMAGE=vke_tunnel
CONTAINER=vke_tunnel
ROSNET=rosnet
#CIP=172.21.0.101
#CGW=172.21.0.1
ROOT_PATH="`dirname \"$0\"`"
ROOT_PATH="`( cd \"$ROOT_PATH\" && pwd )`"
#XPRAPORT=1`id -u`
#HMOUNT=`readlink -f ~`/Work/rosdev
#LMOUNT="/home/user/rosdev"
#NVIDIAEXT="--gpus all"
#LOCAL_USER_ID=`id -u`
HOSTNAME=`hostname`
XTRAOPTS="--privileged -e HOSTNAME=${HOSTNAME}"
#COMMAND="bash"

vke_fw() {
    # Check firewall rules for external connections
    echo "Setup firewall rules"
    brif=`ip a | grep ${CGW} | grep -oE '[^ ]+$'`
    rule=`sudo iptables -t nat -L POSTROUTING -v | grep '[^!]'${brif}`
    if [ -z "$rule" ]; then
        sudo iptables -t nat -I POSTROUTING 1 -o ${brif} -j MASQUERADE
    else
        echo "Rule exists"
    fi
}

vke_start() {
    OPTS="${NVIDIAEXT} --name ${CONTAINER} --hostname ${CONTAINER} --network ${ROSNET}"
    [ ! -z "${HMOUNT}" ] && OPTS="${OPTS} -v ${HMOUNT}:${LMOUNT}"
#    [ ! -z "${CIP}" ] && OPTS="${OPTS} --ip ${CIP}"
    [ ! -z "${LOCAL_USER_ID}" ] && OPTS="${OPTS} -e LOCAL_USER_ID=${LOCAL_USER_ID}"
    docker service create ${OPTS} ${XTRAOPTS} ${IMAGE} ${COMMAND}
#    docker network connect bridge ${CONTAINER}
    docker start ${CONTAINER}
}

vke_stop() {
    docker kill ${CONTAINER}
    docker rm ${CONTAINER}
}

vke_exec() {
    docker exec -it ${CONTAINER} bash
}

case "$1" in

  start)
    echo "Starting ${CONTAINER} docker container"
    vke_start
    echo "Container is running"
    ;;

  stop)
    echo "Stopping ${CONTAINER} docker"
    vke_stop
    ;;

  restart)
    echo "Restartting ${CONTAINER} docker"
    vke_stop
    vke_start
    ;;

  enter)
    echo "Entering ${CONTAINER} docker"
    vke_exec
    ;;

  fw)
    vke_fw
    ;;

  *)
    echo "Usage: $0 start|stop|restart|enter|fw"
    exit 1
    ;;

esac
